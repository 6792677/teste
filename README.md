
## Name of the System
 
Biological Products Manage System

---

## System Function

The System allows to manage all the products in the store.

---

## Development Environment

The project its developed in **Node.js version 14.0.0** using the IDE **Visual Studio Code version 1.44.2** and a test tool called **Postman  version 7.22.1**.

---

## Tutorial to run the system

To run the system follow the next steps:

1. Install node and npm using the command in the command line, **curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash - sudo apt-get install -y nodejs**

2. Clone the repository using the command in the command line, **git clone git@bitbucket.org:7008085/equipa4_api.git**

3. Insert the command in the command line, **npm install**

4. To start the API insert the command in the command line,**node index.js**

---

## How to use the system

To use the system, it is needed to do **GET, PUT, POST and DELETE** requests to the API in the address **localhost:3000**

* List all products using **GET** - http://localhost:3000/products;

* Search for a product by id using **GET** - http://localhost:3000/product/<id\>;

* Insert a product using **POST** and a **body** message - http://localhost:3000/product;

* Update a product using **PUT** and a **body** message - http://localhost:3000/product/<id\>;

* Delete a product using **DELETE** - http://localhost:3000/product/<id\>;

* List all suppliers using **GET** - http://localhost:3000/suppliers;

* Insert a supplier using **POST** and a **body** message - http://localhost:3000/supplier;

* Delete a supplier using **DELETE** - http://localhost:3000/supplier/<id\>;

* Update a supplier using **PUT** and a **body** message - http://localhost:3000/supplier/<id\>;

* Search a product by a supplier Id using **GET** - http://localhost:3000/productBySupplier/<id\>;


Example of **body** in **JSON**:

\{
    "id": 2,
    "product": "Milk",
    "type": "Drink",
    "price": 5,
    "stock": 8
\}

---

## Team Members and their responsabilities

* Diogo Martins - Scrum Master and Developer

* André Louro - Product Owner and Developer	

* Mónica Gonçalves - Developer 

* Renato Farinha - Developer

---

## Implemented Features

* List all products;

* Search for a product by id;

* Insert a product;

* Update a product;

* Delete a product;

* Add type to the object product;

* List all suppliers;

* Insert suppliers;

* Delete suppliers;

* Update suppliers;

* Search product by suppliers;

---

## Build of Dockerfile

To build the Dockerfile execute the command **docker build -t equipa4:v1 .** in the command line.

---

## Run the image produced by the Dockerfile

To run the image created execute the command **docker run --name api-equipa4 -it -d -p 8080:3000 equipa4:v1** in the command line. The exposed port is the 3000 and the host port is 8080.

To use the API access the address **localhost:8080**.

---




