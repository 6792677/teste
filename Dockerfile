FROM node:14

RUN mkdir /equipa4

COPY /Project /equipa4

RUN apt-get update

WORKDIR /equipa4

RUN npm install

EXPOSE 3000

CMD ["node", "index.js"]
