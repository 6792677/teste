module.exports ={ updateProduct: function(products,id,newProduct){
    for (let i = 0; i < products.length; i++) {
        let product = products[i]
        if (product.id === Number(id)) {
            products[i] = newProduct;
        }
    }
    return products;
}}