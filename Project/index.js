
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = 3000;

const addProduct= require("./addProduct.js");
const addSupplier= require("./addSupplier.js")
const findProduct= require('./findProduct.js');
const findProductBySupplier= require('./findProductBySupplier.js');
const deleteProduct = require('./deleteProduct.js');
const deleteSupplier = require('./deleteSupplier.js')
const updateProduct =require('./updateProduct.js');
const updateSuppliers= require('./updateSuppliers.js');

let products = [{id:0, name:"Mini", type: "Drink" , price:5, stock:4},{id:1 , name:"Cornflakes", type:"Cereals",price:5,stock:5}];

let suppliers = [{id:0, name:"Superbock", typeofproducts:"Drink", products:[{name:"Mini", type: "Drink" , price:5, quantity:4},{name:"Litrosa", type: "Drink" , price:5, quantity:4}]}, {id:1, name:"Kellogs", typeofproducts:"Cereals", products:[{name:"Cornflakes", type: "Food" , price:5, quantity:4},{name:"Oatflakes", type: "Food" , price:5, quantity:4}]}]; 

app.use(cors());

// Configuring body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Add all products
app.post('/product', (req, res) => {
    const product = req.body;
    products=addProduct.addProduct(products,product);
    res.send('Product is inserted');
});

//Add all suppliers

app.post('/supplier', (req, res) => {
    const supplier = req.body;
    suppliers=addSupplier.addSupplier(suppliers,supplier);
    res.send('Supplier is inserted');
});

//All products
app.get('/products/', (req, res) => {
    res.json(products);
});

//All suppliers

app.get('/suppliers/', (req, res) => {
    res.json(suppliers);
});


//Find by id
app.get('/product/:id', (req, res) => {
    
    const id = req.params.id;
    res.json(findProduct.findProduct(products,id));

});

//Find by supplier
app.get('/productBySupplier/:id', (req, res) => {
    
    const supplierId = req.params.id;
    res.json(findProductBySupplier.findProductbySupplier(products,suppliers,supplierId));

});

//Delete Products
app.delete('/product/:id', (req, res) => {
    
    const id = req.params.id;
    products = deleteProduct.deleteProduct(products,id)
    res.send("Product Deleted");

});

//Delete Suppliers
app.delete('/supplier/:id', (req, res) => {
    
    const id = req.params.id;
    suppliers = deleteSupplier.deleteSupplier(suppliers,id)
    res.send("Supplier Deleted");

});

//Update Products
app.put('/product/:id', (req, res) => {
    
    
    const id = req.params.id;
    const newProduct = req.body;

    products=updateProduct.updateProduct(products,id,newProduct);

    res.send('Product is edited');
});

//Update Suppliers
app.put('/supplier/:id', (req, res) => {
    
    
    const id = req.params.id;
    const newSupplier = req.body;

    suppliers=updateSuppliers.updateSuppliers(suppliers,id,newSupplier);

    res.send('Supplier is edited');
});

app.listen(port, () => console.log(`API listening on port ${port}!`));
